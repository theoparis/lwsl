plugins {
    kotlin("jvm") version "1.6.10"

    `java-library`
    `maven-publish`
}

apply(plugin = "org.jetbrains.kotlin.jvm")

repositories {
    mavenCentral()
    mavenLocal()
    maven("https://jitpack.io")
}

dependencies {
    api("com.gitlab.theoparis:event-bus:master-SNAPSHOT")
    implementation("com.github.throw-out-error:toe-utils:master-SNAPSHOT")
    api("io.projectreactor.netty:reactor-netty:1.0.5")
    testImplementation("junit:junit:4.12")
    implementation("com.google.code.gson:gson:2.8.6")
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId = project.properties["groupId"] as String
            artifactId = project.properties["artifactId"] as String
            version = project.properties["version"] as String

            from(components["java"])
        }
    }
}
