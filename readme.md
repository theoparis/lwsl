# Light Weight Socket Library

LWSL is an open source socket library made in Java using JSON packets.  
It is used to create small backend applications such as verification servers, rats or other simple tasks.  
Everyone can contribute by making pull requests.  
If you have any issues / new features, present them in the issues tab.

## Usage

## Contributors

- [Baddeveloper (Stijn Simons)](https://github.com/StijnSimons)
- [arankin-irl (Alex Rankin)](https://github.com/arankin-irl)
- [Theo Paris](https://gitlab.com/theoparis)
